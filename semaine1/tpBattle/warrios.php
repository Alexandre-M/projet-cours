<?php

class Warrios{

    public $life;
    private $name;
    private $weapon;

    function __construct($name, $life, $weapon){
        $this->name = $name;
        $this->life = $life;
        $this->weapon = $weapon;
    }

    function getName(){
        return $this->name;
    }

    function getLife(){
        return $this->life;
    }

    function statue(){
        echo $this->name." rest ".$this->life." life.\n";
        $this->weapon->weaponHand();
    }

    function attack($ennemi){
        $ennemi->hit($this->weapon->getDamage());

        echo $this->name." attack ".$ennemi->getName()." with for ".$this->weapon->getName()." and take ".$this->weapon->getDamage()." damage.\n";

        if ($ennemi->getLife() > 0) {
            echo $ennemi->getName()." rest ".$ennemi->getLife()." life.\n";
        }

        else {
            echo $ennemi->getName()." is dead, ".$this->name." is winner with ".$this->life. " life !\n";
        }
    }

    function hit($damage){
        $this->life = $this->life - $damage;
        return $this->life;
    }
}