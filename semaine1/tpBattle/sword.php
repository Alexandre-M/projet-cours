<?php

require_once("./weapon.php");

class sword extends weapon{

    function __construct($nameWeapon, $damage){
        parent::__construct($nameWeapon, $damage);
    }

    function weaponHand(){
        echo "This weapon is ".$this->nameWeapon." with damage ".$this->damage.".\n";
    }
}