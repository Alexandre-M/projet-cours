<?php
    class Arme{
        private $type;
        private $poids;
        private $longueur;
        protected $degats;
        protected $enchant = [];

        public function __construct($type, $poids, $degats, $longueur)
        {
            $this->type = $type
            $this->poids = $poids
            $this->degats = $degats
            $this->longueur = $longueur
        };

        public function addEnchant($enchantName)
        {
            array_push($this->enchantName, $enchantName);
            return $this;
        }

        public function getType(){
            return $this->type
        }

        public function getPoids(){
            return $this->poids
        }

        public function getDegats(){
            return $this->degats
        }

        public function getLongueur(){
            return $this->longueur
        }

        public function getEnchant(){
            return $this->enchant
        }

        public function attack($life){
            return $life - $this->degats
        }
    }   

    class Arme_melee extends Arme{
    }

    class Arme_distance extends Arme{
        private $porter

        public function __construct($porter, $degats){ 
            parent::__construct($degats);
            $this->porter = $porter;
        }

        public function porterLancer($ennemiDistance){
            if ($ennemiDistance < $this->porter) {
                echo "La cible à été atteinte lui infligent " .$this->degats " dégats."
            }

            else {
                echo "la cible est trop loin, dommage."
            }
        }
    }

    $arme = new Arme("Wood", 10, 10, 10);
    $arme->attack(100);
?>