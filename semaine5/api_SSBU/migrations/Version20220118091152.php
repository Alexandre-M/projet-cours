<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220118091152 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE combat (id INT AUTO_INCREMENT NOT NULL, personnage_joueur_id INT NOT NULL, personnage_adversaire_id INT NOT NULL, date DATETIME NOT NULL, a_gagner SMALLINT NOT NULL, adversaire VARCHAR(45) NOT NULL, INDEX IDX_8D51E39898FB72AC (personnage_joueur_id), INDEX IDX_8D51E3985D66515A (personnage_adversaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE coup_speciaux (id INT AUTO_INCREMENT NOT NULL, id_personnage_id INT NOT NULL, name VARCHAR(45) NOT NULL, pourcantages VARCHAR(45) NOT NULL, direction VARCHAR(45) NOT NULL, INDEX IDX_79ED4EEAE0198227 (id_personnage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personnage (id INT AUTO_INCREMENT NOT NULL, id_univers_id INT NOT NULL, name VARCHAR(45) NOT NULL, link_image VARCHAR(45) NOT NULL, INDEX IDX_6AEA486DB97C9CDC (id_univers_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE univers (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(45) NOT NULL, link_image VARCHAR(45) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE combat ADD CONSTRAINT FK_8D51E39898FB72AC FOREIGN KEY (personnage_joueur_id) REFERENCES personnage (id)');
        $this->addSql('ALTER TABLE combat ADD CONSTRAINT FK_8D51E3985D66515A FOREIGN KEY (personnage_adversaire_id) REFERENCES personnage (id)');
        $this->addSql('ALTER TABLE coup_speciaux ADD CONSTRAINT FK_79ED4EEAE0198227 FOREIGN KEY (id_personnage_id) REFERENCES personnage (id)');
        $this->addSql('ALTER TABLE personnage ADD CONSTRAINT FK_6AEA486DB97C9CDC FOREIGN KEY (id_univers_id) REFERENCES univers (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE combat DROP FOREIGN KEY FK_8D51E39898FB72AC');
        $this->addSql('ALTER TABLE combat DROP FOREIGN KEY FK_8D51E3985D66515A');
        $this->addSql('ALTER TABLE coup_speciaux DROP FOREIGN KEY FK_79ED4EEAE0198227');
        $this->addSql('ALTER TABLE personnage DROP FOREIGN KEY FK_6AEA486DB97C9CDC');
        $this->addSql('DROP TABLE combat');
        $this->addSql('DROP TABLE coup_speciaux');
        $this->addSql('DROP TABLE personnage');
        $this->addSql('DROP TABLE univers');
    }
}
