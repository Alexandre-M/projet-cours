<?php

namespace App\Controller;

use App\Entity\CoupSpeciaux;
use App\Form\CoupSpeciauxType;
use App\Repository\CoupSpeciauxRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/coup/speciaux")
 */
class CoupSpeciauxController extends AbstractController
{
    /**
     * @Route("/", name="coup_speciaux_index", methods={"GET"})
     */
    public function index(CoupSpeciauxRepository $coupSpeciauxRepository): Response
    {
        return $this->render('coup_speciaux/index.html.twig', [
            'coup_speciauxes' => $coupSpeciauxRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="coup_speciaux_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $coupSpeciaux = new CoupSpeciaux();
        $form = $this->createForm(CoupSpeciauxType::class, $coupSpeciaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($coupSpeciaux);
            $entityManager->flush();

            return $this->redirectToRoute('coup_speciaux_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('coup_speciaux/new.html.twig', [
            'coup_speciaux' => $coupSpeciaux,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="coup_speciaux_show", methods={"GET"})
     */
    public function show(CoupSpeciaux $coupSpeciaux): Response
    {
        return $this->render('coup_speciaux/show.html.twig', [
            'coup_speciaux' => $coupSpeciaux,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="coup_speciaux_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, CoupSpeciaux $coupSpeciaux, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CoupSpeciauxType::class, $coupSpeciaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('coup_speciaux_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('coup_speciaux/edit.html.twig', [
            'coup_speciaux' => $coupSpeciaux,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="coup_speciaux_delete", methods={"POST"})
     */
    public function delete(Request $request, CoupSpeciaux $coupSpeciaux, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$coupSpeciaux->getId(), $request->request->get('_token'))) {
            $entityManager->remove($coupSpeciaux);
            $entityManager->flush();
        }

        return $this->redirectToRoute('coup_speciaux_index', [], Response::HTTP_SEE_OTHER);
    }
}
