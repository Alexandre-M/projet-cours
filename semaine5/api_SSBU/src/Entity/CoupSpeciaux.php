<?php

namespace App\Entity;

use App\Repository\CoupSpeciauxRepository;
use App\Entity\Univers;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CoupSpeciauxRepository::class)
 */
class CoupSpeciaux
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $pourcantages;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $direction;

    /**
     * @ORM\ManyToOne(targetEntity=Personnage::class, inversedBy="coupSpeciauxes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idPersonnage;

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPourcantages(): ?string
    {
        return $this->pourcantages;
    }

    public function setPourcantages(string $pourcantages): self
    {
        $this->pourcantages = $pourcantages;

        return $this;
    }

    public function getDirection(): ?string
    {
        return $this->direction;
    }

    public function setDirection(string $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    public function getIdPersonnage(): ?Personnage
    {
        return $this->idPersonnage;
    }

    public function setIdPersonnage(?Personnage $idPersonnage): self
    {
        $this->idPersonnage = $idPersonnage;

        return $this;
    }
}
