<?php

namespace App\Entity;

use App\Repository\PersonnageRepository;
use App\Entity\Univers;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass=PersonnageRepository::class)
 */
class Personnage implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $link_image;

    /**
     * @ORM\ManyToOne(targetEntity=univers::class, inversedBy="personnages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idUnivers;

    /**
     * @ORM\OneToMany(targetEntity=CoupSpeciaux::class, mappedBy="idPersonnage")
     */
    private $coupSpeciauxes;

    /**
     * @ORM\OneToMany(targetEntity=Combat::class, mappedBy="personnage_joueur")
     */
    private $combats;

    public function __construct()
    {
        $this->coupSpeciauxes = new ArrayCollection();
        $this->combats = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLinkImage(): ?string
    {
        return $this->link_image;
    }

    public function setLinkImage(string $link_image): self
    {
        $this->link_image = $link_image;

        return $this;
    }

    public function getIdUnivers(): ?univers
    {
        return $this->idUnivers;
    }

    public function setIdUnivers(?univers $idUnivers): self
    {
        $this->idUnivers = $idUnivers;

        return $this;
    }

    /**
     * @return Collection|CoupSpeciaux[]
     */
    public function getCoupSpeciauxes(): Collection
    {
        return $this->coupSpeciauxes;
    }

    public function addCoupSpeciaux(CoupSpeciaux $coupSpeciaux): self
    {
        if (!$this->coupSpeciauxes->contains($coupSpeciaux)) {
            $this->coupSpeciauxes[] = $coupSpeciaux;
            $coupSpeciaux->setIdPersonnage($this);
        }

        return $this;
    }

    public function removeCoupSpeciaux(CoupSpeciaux $coupSpeciaux): self
    {
        if ($this->coupSpeciauxes->removeElement($coupSpeciaux)) {
            // set the owning side to null (unless already changed)
            if ($coupSpeciaux->getIdPersonnage() === $this) {
                $coupSpeciaux->setIdPersonnage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Combat[]
     */
    public function getCombats(): Collection
    {
        return $this->combats;
    }

    public function addCombat(Combat $combat): self
    {
        if (!$this->combats->contains($combat)) {
            $this->combats[] = $combat;
            $combat->setPersonnageJoueur($this);
        }

        return $this;
    }

    public function removeCombat(Combat $combat): self
    {
        if ($this->combats->removeElement($combat)) {
            // set the owning side to null (unless already changed)
            if ($combat->getPersonnageJoueur() === $this) {
                $combat->setPersonnageJoueur(null);
            }
        }

        return $this;
    }

    public function jsonPersonnages(){

        $personnagesArray = [];
        foreach($this->personnages as $personnage){
            $personnagesArray[] = $personnage->jsonSerialize();
        }
        return $personnagesArray;
    }

    public function jsonSerialize()
    {
        return
        [
            'id' => $this->getId(),
            'name'  => $this->getName(),
            'link_image'  => $this->getLinkImage(),
            'personnages'  => $this->jsonPersonnages(),
        ];
    }

    public function jsonSerializeList()
    {
        return
        [
            'id' => $this->getId(),
            'name'  => $this->getName(),
            'link_image'  => $this->getLinkImage(),
            //'personnagesCount'  => count($this->personnages),
        ];
    }
}
