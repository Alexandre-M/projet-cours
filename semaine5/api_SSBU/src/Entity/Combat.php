<?php

namespace App\Entity;

use App\Repository\CombatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CombatRepository::class)
 */
class Combat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="smallint")
     */
    private $a_gagner;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $adversaire;

    /**
     * @ORM\ManyToOne(targetEntity=Personnage::class, inversedBy="combats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $personnage_joueur;

    /**
     * @ORM\ManyToOne(targetEntity=personnage::class, inversedBy="combats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $personnage_adversaire;

    public function __toString()
    {
        return $this->date;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAGagner(): ?int
    {
        return $this->a_gagner;
    }

    public function setAGagner(int $a_gagner): self
    {
        $this->a_gagner = $a_gagner;

        return $this;
    }

    public function getAdversaire(): ?string
    {
        return $this->adversaire;
    }

    public function setAdversaire(string $adversaire): self
    {
        $this->adversaire = $adversaire;

        return $this;
    }

    public function getPersonnageJoueur(): ?Personnage
    {
        return $this->personnage_joueur;
    }

    public function setPersonnageJoueur(?Personnage $personnage_joueur): self
    {
        $this->personnage_joueur = $personnage_joueur;

        return $this;
    }

    public function getPersonnageAdversaire(): ?personnage
    {
        return $this->personnage_adversaire;
    }

    public function setPersonnageAdversaire(?personnage $personnage_adversaire): self
    {
        $this->personnage_adversaire = $personnage_adversaire;

        return $this;
    }
}
