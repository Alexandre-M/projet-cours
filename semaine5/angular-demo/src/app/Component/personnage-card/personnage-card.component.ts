import {Component, Input, OnInit} from '@angular/core';
import Personnage from "../../Type/Personnage";

@Component({
  selector: 'personnage-card',
  templateUrl: './personnage-card.component.html',
  styleUrls: ['./personnage-card.component.css']
})
export class PersonnageCardComponent implements OnInit {

  @Input() personnage: Personnage | null = null

  constructor() { }

  ngOnInit(): void {
  }

}
