import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UniversCardComponent } from './Component/univers-card/univers-card.component';
import { UniversPageComponent } from './Pages/univers-page/univers-page.component';
import { UniversShowComponent } from './Pages/univers-show/univers-show.component';
import { PersonnageCardComponent } from './Component/personnage-card/personnage-card.component';
import { EditUniversComponent } from './Pages/edit-univers/edit-univers.component';
import { DeleteUniversComponent } from './Pages/delete-univers/delete-univers.component';
import {ReactiveFormsModule} from "@angular/forms";
import { UniversCreationComponent } from './Pages/univers-creation/univers-creation.component';

@NgModule({
  declarations: [
    AppComponent,
    UniversCardComponent,
    UniversPageComponent,
    UniversShowComponent,
    PersonnageCardComponent,
    EditUniversComponent,
    DeleteUniversComponent,
    UniversCreationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
