<?php

class subject
{
    /**
    * @var observeur[]
    */
    private $observers;

    /**
    * @var string
    */
    private $name;

    /**
     * @param string $name
     */
    function __construct($name){
        $this->observers = [];
        $this->name = $name;
    }

    /**
     * @param string $message
     */
    function onNotify($message){
        foreach ($this->observer as $observers) {
            $observer->onNotify($message);
        }
    }
    
}
