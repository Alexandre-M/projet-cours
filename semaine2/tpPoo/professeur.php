<?php

require_once("./personne.php");
require_once("./eleve.php");
require_once("./maison.php");
require_once("./matiere.php");

class Professeur extends Personne{

    private $matiere;
    
    function __construct($nom, $genre, $maison, $matiere){
        parent::__construct($nom, $genre, $maison);
        $this->matiere = $matiere;
    }

    public function getMatiere(){
        return $this->matiere->getName();
    }

    public function donnerCours(){
        echo ("Le professeur ".$this->nom." donne des cours de ".$this->matiere.".\n");
    }

    public function donnerRetenue($eleve){
        echo ("Une retenu a été donné à".$eleve->getName()." par ".$this->nom.".\n");
    }

    public function ajouterPoint($nbPoint, $maison){
        if ($nbPoint>100){
            echo ("Les points donnés sont trop impotant\n");
        }
        else{
            $maison->ajoutPoint($nbPoint);
        }
    }

    public function supprimerPoint($nbPoint, $maison){
        if ($nbPoint>100){
            echo ("Les points enlevés sont trop impotant\n");
        }
        else{
            $maison->retraitPoint($nbPoint);
        }
    }





    public function nommerPrefet($eleve){
        if($eleve->getAnnee() < 5){
            echo ("L'éléve n'est pas encore en 5ème année'.\n");
        }
        $eleve->getMaison()->nommagePrefet($eleve);
    }

}
