<?php

require_once("./ecole.php");

class Maison extends Ecole{

    private $nomMaison;
    private $nbPoint;
    private $prefetG;
    private $prefetF;
    private $directeurMaison;
    
    function __construct($nomEcole, $nomMaison){
        parent::__construct($nomEcole);
        $this->nomMaison = $nomMaison;
        $this->nbPoint = 0;
        $this->directeurMaison = null;
        $this->prefetG = null;
        $this->prefetF = null;
    }

    public function getName(){
        return $this->nomMaison;
    }

    public function getPrefetG(){
        if ($this->prefetG == null) {
            return "Personne";
        }

        else {
            return $this->prefetG->getName();
        }
    }

    public function getPrefetF(){
        if ($this->prefetF == null) {
            return "Personne";
        }

        else {
            return $this->prefetF->getName();
        }
    }

    public function getDirecteurMaison(){
        if ($this->directeurMaison == null) {
            return "Personne";
        }

        else {
            return $this->directeurMaison->getName();
        }
    }





    public function nommageDirecteurMaison($professeur){
        $this->directeurMaison = $professeur;
        echo ("Le professeur ".$professeur->getName()." est devenu le nouveau directeur de la maison ".$this->nomMaison.".\n");
    }

    public function nommagePrefet($eleve){
        if ($eleve->getName() == "G"){
            $this->prefetG = $eleve;
        }
        else {$this->prefetF = $eleve;};

        echo ("L'éléve ".$eleve->getName()." est devenu le nouveau prefet de la maison ".$this->nomMaison.".\n");
    }





    public function ajoutPoint($point){
        $this->nbPoint = $this->nbPoint + $point;
        echo ("La maison ".$this->nomMaison." à gagné ".$point." points, elle a un total de ".$this->nbPoint." points !\n");
    }

    public function retraitPoint($point){
        $this->nbPoint = $this->nbPoint - $point;
        echo ("La maison ".$this->nomMaison." à perdu ".$point." points, elle a un total de ".$this->nbPoint." points.\n");
    }

}
