<?php

class Matiere{

    /**
     * @property string
     */
    private $nomMatiere;
    
    function __construct($nomMatiere){
        $this->nomMatiere = $nomMatiere;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->nomMatiere;
    }

}
