<?php

abstract class Ecole{

    protected $nomEcole;
    protected $directeur;
    protected $prefetChefG;
    protected $prefetChefF;
    
    function __construct($nomEcole){
        $this->nomEcole = $nomEcole;
        $this->directeur = null;
        $this->prefetChefG = null;
        $this->prefetChefF = null;
    }

    public function nommagePrefetChef($eleve){
        if ($eleve->getName() == "G"){
            $this->prefetChefG = $eleve;
        }
        else {$this->prefetChefF = $eleve;};
        
        echo ("L'éléve ".$eleve->getName()." est devenu le nouveau prefet en chef de l'école ".$this->nomEcole."\n");
    }
}
