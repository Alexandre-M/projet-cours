<?php

require_once("./personne.php");

class Eleve extends Personne{

    private $annee;
    
    function __construct($nom, $genre, $maison, $annee){
        parent::__construct($nom, $genre, $maison);
        $this->annee = $annee;
    }

    public function getAnnee(){
        return $this->annee;
    }

}
