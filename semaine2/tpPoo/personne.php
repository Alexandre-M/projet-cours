<?php

require_once("./maison.php");

abstract class Personne{

    protected $nom;
    protected $genre;
    protected $maison;
    
    function __construct($nom, $genre, $maison){
        $this->nom = $nom;
        $this->genre = $genre;
        $this->maison = $maison;
    }

    public function getName(){
        return $this->nom;
    }

    public function getGenre(){
        return $this->genre;
    }

    public function getMaison(){
        return $this->maison;
    }

}
