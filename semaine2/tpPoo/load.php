<?php

require_once("./maison.php");
require_once("./matiere.php");
require_once("./directeur.php");
require_once("./professeur.php");
require_once("./eleve.php");

$maisonGryffondor = new Maison("Poudlard", "Gryffondor");
$maisonPoufsouffle = new Maison("Poudlard", "Poufsouffle");
$maisonSerdaigle = new Maison("Poudlard", "Serdaigle");
$maisonSerpentard = new Maison("Poudlard", "Serpentard");

$matierePotions = new Matiere("Potions");
$matiereRune = new Matiere("Rune");
$matiereAstronomie = new Matiere("Astronomie");
$matiereDefense = new Matiere("Défense contre les forces du mal");
$matiereSortileges = new Matiere("Sortilèges");
$matiereDivination = new Matiere("Divination");
$matiereBotanique = new Matiere("Botanique");
$matiereSoins = new Matiere("Soins aux créatures magique");

$directrice = new Directeur("Hermione GRANGER", "F", $maisonGryffondor);

$professeurHagrid = New Professeur("Hagrid", "G", $maisonGryffondor, $matiereSoins);

$eleveMalfoy = new Eleve("Malfoy", "G", $maisonSerpentard, "6");
$elevePotter = new Eleve("Potter", "G", $maisonGryffondor, "7");

$eleveFantome = new Eleve("Fantôme", "F", $maisonSerpentard, "2");
