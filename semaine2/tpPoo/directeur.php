<?php

require_once("./personne.php");
require_once("./professeur.php");
require_once("./eleve.php");
require_once("./maison.php");

class Directeur extends Personne{
    
    function __construct($nom, $genre, $maison){
        parent::__construct($nom, $genre, $maison);
    }




    public function renvoyer($personne){
        $nom = $personne->getName();
        unset($personne);
        echo($nom." a été supprimé des fichiers.\n");
    }




    public function nommerDirecteurMaison($professeur){
        if($professeur->getMatiere() == null){
            echo ("La personne choisi n'est pas un professeur.\n");
        }
        $professeur->getMaison()->nommageDirecteurMaison($professeur);
    }

    public function nommerPrefetChef($eleve){
        if($eleve->getAnnee() <> 7){
            echo ("L'éléve n'est pas en 7ème année'.\n");
        }
        $eleve->getMaison()->nommagePrefetChef($eleve);
    }

    public function nommerPrefet($eleve){
        if($eleve->getAnnee() < 5){
            echo ("L'éléve n'est pas encore en 5ème année'.\n");
        }
        $eleve->getMaison()->nommagePrefet($eleve);
    }





    public function donnerRetenue($eleve){
        echo ("Une retenu a été donné à".$eleve->getName().".\n");
    }





    public function ajouterPoint($nbPoint, $maison){
        if ($nbPoint>200){
            echo ("Les points donnés sont trop impotant\n");
        }
        else{
            $maison->ajoutPoint($nbPoint);
        }
    }

    public function supprimerPoint($nbPoint, $maison){
        if ($nbPoint>250){
            echo ("Les points enlevés sont trop impotant\n");
        }
        else{
            $maison->retraitPoint($nbPoint);
        }
    }

}
