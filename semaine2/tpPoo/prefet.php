<?php

require_once("./eleve.php");
require_once("./maison.php");

class Prefet extends Eleve{

    private $chef;
    
    function __construct($nom, $genre, $maison, $annee){
        parent::__construct($nom, $genre, $maison, $annee);
        $this->chef = false;
    }

    public function ajouterPoint($nbPoint, $maison){
        if ($nbPoint>50){
            echo ("Les points donnés sont trop impotant\n");
        }

        else if ($nbPoint>25 && $this->chef = false){
            echo ("Les points donnés sont trop impotant\n");
        }

        else{
            $maison->ajoutPoint($nbPoint);
        }
    }

    public function supprimerPoint($nbPoint, $maison){
        if ($nbPoint>50){
            echo ("Les points enlevés sont trop impotant\n");
        }

        else if ($nbPoint>25 && $this->chef = false){
            echo ("Les points donnés sont trop impotant\n");
        }

        else{
            $maison->retraitPoint($nbPoint);
        }
    }

}
