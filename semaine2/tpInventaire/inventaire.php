<?php

require("./consommable.php");
require("./arme/arc.php");
require("./arme/epee.php");
require("./armure/tete.php");
require("./armure/torse.php");
require("./armure/jambes.php");
require("./armure/pieds.php");

class Inventaire
{
    function generatate(){
        //$liste = new arc('arc', 22, 'arc');
        //echo $liste->getArme()."\n";

        $arc = new arc('Braindille avec ficelle', 22, 'Arc');
        $epee = new epee('Lame en bois', 30, 'Epee');
        $tete = new tete('Chapeau de paille', 5, 'Haut');
        $torse = new torse('Cotte de fer', 18, 'Torse');
        $jambes = new jambes('Short de plage', 7, 'Bas');
        $pieds = new pieds('Chaussure à pointe', 50, 'Pieds');
        $item1 = new consommable('Potion', 'Renforce force +20');
        $item2 = new consommable('Eau naturelle', 'Rien');

        echo "
        ==[INVENTAIRE]==
        ===[ARMURES]===
        PIED = ".$pieds->getName()."
        JAMBES = ".$jambes->getName()."  
        TORSE = ".$torse->getName()."
        TETE = ".$tete->getName()."
        ===[ARME]===
        EPEE = ".$epee->getName()."
        ARC = ".$arc->getName()."
        ===[AUTRES]===
        ITEM = ".$item1->getName()."\n";
    }
}
