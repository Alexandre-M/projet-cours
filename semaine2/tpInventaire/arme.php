<?php

require_once("./objets.php");

abstract class Arme extends Objets
{
    protected $pointAtt;
    protected $categorie;

    function __construct($name, $pointAtt, $categorie){
        parent::__construct($name);
        $this->pointAtt = $pointAtt;
        $this->categorie = $categorie;
    }

    function getArme(){
        $description = "L'arme ".$this->name." fait des dégats de ".$this->pointAtt." et elle est de du type ".$this->categorie;
        return $description;
    }
}
