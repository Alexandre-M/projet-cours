<?php

require_once("./objets.php");

class Consommable extends Objets{

    public $effet;

    function __construct($name, $effet){
        parent::__construct($name);
        $this->effet = $effet;
    }

}