<?php

require_once("./objets.php");

abstract class Armure extends Objets
{
    protected $pointDef;
    protected $categorie;

    function __construct($name, $pointDef, $categorie){
        parent::__construct($name);
        $this->pointDef = $pointDef;
        $this->categorie = $categorie;
    }
}
