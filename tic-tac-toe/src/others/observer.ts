export class Observer {
    
    private name: string
    private callback: Function | null

    constructor(name: string, callback: Function | null = null){
        this.name = name
        this.callback = callback
    }

    setCallback(callback: Function): Observer{
        this.callback = callback
        return this
    }

    onNotify(): void{
        console.log(`[${this.name}] is notified !`)
        if(this.callback != null){
            this.callback()
        }
    }


}