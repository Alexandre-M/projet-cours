import { Observer } from "./observer";

export class Subject{
    
    name: string;
    observers: Observer[];

    constructor(name: string){
        this.name = name;
        this.observers = []
    }

    addObserver(observer: Observer): Subject{
        this.observers.push(observer)
        return this
    }

    notify(){
        this.observers.forEach((observer) => {
            observer.onNotify()
        })
    }
}