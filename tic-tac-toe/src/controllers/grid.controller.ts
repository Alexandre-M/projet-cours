//import { GridModel } from "../models/grid.model";
import { Observer } from "../others/observer";
import { Subject } from "../others/subject";
import { GridView } from "../views/grid.view";

export class GridController {

    //private gridModel: GridModel;
    private gridView: GridView;
    private gridViewObserver: Observer;

    public i: number;
    gridSubject: Subject;


    constructor(){
        this.i = 0
        // this.gridModel = new GridModel()

        this.gridSubject = new Subject("gridSubject")
        this.gridView = new GridView(this)

        this.gridViewObserver = new Observer("gridViewObserverController")
        this.gridViewObserver.setCallback(() => {this.update()})
        this.gridView.getViewSubject().addObserver(this.gridViewObserver)
    }

    update() {
        this.i++
        console.log(this.i)
        this.gridSubject.notify()
    }
}