import { GridController } from "../controllers/grid.controller";
import { HTMLRenderer } from "../HTMLRenderer";
import { Observer } from "../others/observer";
import { Subject } from "../others/subject";

export class GridView{

    private viewSubject: Subject;
    private gridParent: HTMLDivElement;
    private gridContenant: HTMLDivElement;
    private gridController: GridController;
    private gridViewObserver: Observer;

    constructor(gridController: GridController){

        this.gridController = gridController

        this.gridViewObserver = new Observer("gridViewObserverView")
        this.gridViewObserver.setCallback(() => {
            this.render()
        })
        this.gridController.gridSubject.addObserver(this.gridViewObserver)


        this.viewSubject = new Subject("viewSubject");
    
        this.gridParent = HTMLRenderer.getRenderer().createDiv()
        this.gridParent.addEventListener('click', () => {
            this.viewSubject.notify()
        })
        this.gridContenant = HTMLRenderer.getRenderer().createGrid(this.gridParent);
        HTMLRenderer.getRenderer().getAppParent()?.appendChild(this.gridContenant)
    }

    getViewSubject(){
        return this.viewSubject
    }

    render(){
        this.gridParent.innerText = this.gridController.i.toString()
    }

}