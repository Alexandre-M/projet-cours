export class HTMLRenderer {

    static instance: null | HTMLRenderer;

    static getRenderer(){
        if(this.instance == null){
            this.instance = new HTMLRenderer()
        }
        return this.instance
    }

    getAppParent(): HTMLElement | null{
        return document.getElementById("app")
    }

    createGrid(div: HTMLDivElement): HTMLDivElement{
        let table = document.createElement("div");
        table.style.display = "wrap";
        table.style.width = "350px";
        for (let i=0; i<9; i++){
            table.appendChild(div);
            console.log(i);
        }
        return table
    }

    createDiv(): HTMLDivElement{
        let div = document.createElement("div");
        div.style.height = "100px";
        div.style.width = "100px";
        div.style.backgroundColor = "yellow";
        div.style.color = "black";
        div.style.fontSize = "3em";
        return div
    }

    

}